### Hi there, I'm Julien-Maxime, or Max! 👋

<img src="https://gitlab.com/jmfergeau/jmfergeau/raw/main/avatar.png" align="right" height="150px">
I'm a passionate french self-learning frontend webdev actually looking for a job. I'm also a software translator french-english, an opensource lover and I develop videogames as a hobby.

I also have a [GitHub](https://github.com/jmfergeau) but my Gitlab is more active.

I'm in an urgent need of a job, so please consider checking [📓 My portfolio](https://jmf-portfolio.netlify.com/)!

<p align="center"><i>Thanks for stopping by!</i> 🙏</p>
