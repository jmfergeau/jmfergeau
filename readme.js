module.exports = `### Hi there, I'm Julien-Maxime, or Max! 👋

<img src="https://github.com/maxlefou/maxlefou/raw/master/avatar.png" align="right" height="150px">
I'm a passionate french self-learning frontend webdev actually looking for a job. I'm also a software translator french-english, an opensource lover and I develop videogames as a hobby.

Note that most of my activity is on [Gitlab](https://gitlab.com/maxlefou) and that I only share here my public/opensource works which are actually Gitlab mirror repositories.

I'm in an urgent need of a job, so please consider checking [📓 My portfolio](https://jmf-portfolio.netlify.com/)!

<p align="center"><i>Thanks for stopping by!</i> 🙏</p>

### Visitor count

<img src="https://profile-counter.glitch.me/jmfergeau/count.svg" />

Last update on <#today_date>

<#day_before_new_years> days before new years

<#gabot_signing>
`;